$(document).ready(function () {
    var slideImages = [{
            src: 'img/glass.jpeg'
        },
        {
            src: 'img/klaks.jpg'
        },
        {
            src: 'img/notebook.jpg'
        },
        {
            src: 'img/pcdemon.jpg'
        },
        {
            src: 'img/zte.jpg'
        }
    ]
    //var jR3DCarousel;

    jR3DCarousel = $('.jR3DCarouselGallery').jR3DCarousel({
        width: 470,
        /* ширина для слайдера по умолчанию */
        height: 272,
        /* высота для слайдера по умолчанию */
        slides: slideImages /* массив со ссылками на картинки */
    });


    // Параметры для Кастомной версии слайдера
    var carouselCustomeTemplateProps = {
        width: 600,
        /* largest allowed width */
        height: 400,
        /* largest allowed height */
        slideLayout: 'fill',
        /* "contain" (fit according to aspect ratio), "fill" (stretches object to fill) and "cover" (overflows box but maintains ratio) */
        animation: 'slide3D',
        /* slide | scroll | fade | zoomInSlide | zoomInScroll */
        animationCurve: 'ease',
        animationDuration: 1900,
        animationInterval: 2000,
        slideClass: 'jR3DCarouselCustomSlide',
        autoplay: false,
        controls: true,
        /* control buttons */
        navigation: '' /* circles | squares | '' */ ,
        perspective: 2200,
        rotationDirection: 'ltr',
        onSlideShow: slideShownCallback
    }

    function slideShownCallback($slide) {
        console.log("Slide shown: ", $slide.find('img').attr('src'))
    }

    // устанавливаем Кастомные параметры для слайдера с определенным классом
    jR3DCarouselCustomeTemplate = $('.jR3DCarouselGalleryCustomeTemplate').jR3DCarousel(
        carouselCustomeTemplateProps);

})